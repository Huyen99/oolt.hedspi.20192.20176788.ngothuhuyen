//Example 4: Triangle
import java.util.Scanner;
public class Ex4_Triangle {
	public static void main(String args[]) {
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Enter the height of the triangle: ");
		int iHeight = keyboard.nextInt();
		
		for(int i = 1; i <= iHeight; i++) {
			for (int j = 0; j <= (2*iHeight - 1); j++) {
				if(j - i == iHeight - 1) {
					System.out.println("\n");
					break;
				}
				if(j >= iHeight - i) System.out.print("*");
				else System.out.print(" ");
			}
		}	
	}
}
