//Example 5: Number of days in months

import java.util.Scanner;
public class Ex5_NumberOfDays {
	public static void main(String args[]) {
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Enter the year: ");
		int iYear = keyboard.nextInt();
		System.out.println("Enter the month: ");
		int iMonth = keyboard.nextInt();
		
		System.out.print("The number of days on that month: ");
		if (iMonth == 1||iMonth == 3||iMonth == 5||iMonth == 7||iMonth == 8||iMonth == 10||iMonth == 12) System.out.print("31 days.");
		else if (iMonth == 2) {
			if(iYear % 4 == 0) System.out.print("29 days.");
			else System.out.print("28 days.");
		}
		else System.out.print("30 days.");
	}
}