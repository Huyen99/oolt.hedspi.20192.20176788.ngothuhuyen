//Example 3: Hello Name Dialog
import javax.swing.JOptionPane;
public class Ex3_HelloNameDialog {
	public static void main(String args[]) {
		String result = JOptionPane.showInputDialog("Please enter your name:");
		JOptionPane.showMessageDialog(null, "Hi " + result + "!");
		System.exit(0);
	}
}
