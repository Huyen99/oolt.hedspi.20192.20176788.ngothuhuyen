
//Example 4: Show two numbers
import javax.swing.JOptionPane;

public class Ex4_ShowTwoNumbers {
	public static void main(String args[]) {
		String strNum1, strNum2;
		String strNotification = "You've just entered two numbers: ";

		strNum1 = JOptionPane.showInputDialog(null, "Please input the first number: ", "Input the first number", JOptionPane.INFORMATION_MESSAGE);
		strNum2 = JOptionPane.showInputDialog(null, "Please input the second number: ", "Input the second number", JOptionPane.INFORMATION_MESSAGE);
		
		JOptionPane.showMessageDialog(null, strNotification + strNum1 + " and " + strNum2, "Show two numbers", JOptionPane.INFORMATION_MESSAGE);
		System.exit(0);
	}
}
