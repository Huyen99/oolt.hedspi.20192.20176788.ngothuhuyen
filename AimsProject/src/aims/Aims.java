package aims;

import java.util.Scanner;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Iterator;
import java.util.Collections;

import order.Order;
import media.Book;
import media.CompactDisc;
import media.DigitalVideoDisc;
import media.Media;
import media.Track;

public class Aims {
	
	
	public static void main(String[] args) {

		
		// Create a deamon thread
		MemoryDaemon memoryDaemon = new MemoryDaemon();
		Thread t = new Thread(memoryDaemon);
		t.setDaemon(true);
		t.start();
		
		
		Order testOrder = new Order();
		Scanner keyboard = new Scanner(System.in);
		String title = "";			// Media
		String category = "";
		float cost = 0;
		List<CompactDisc> collection = new ArrayList<CompactDisc>();

		int choice;
		do {
			
			showMenu();
			choice = Integer.parseInt(keyboard.nextLine()) ;
			switch(choice) {
			case 1:		// create new order
				testOrder = new Order();
				break;
			case 2:		// add items

				String author = "";			// Book
				int numberAuthors = 0;
				List<String> authors = new ArrayList<String>();
				
				int numberTracks = 0;	// Compact Disc
				int length = 0;
				//String artist = "";
				String director = "";
				
				do {
					System.out.println("Kind of item?");
					System.out.println("1. Book");
					System.out.println("2. Compact Disc");
					System.out.println("3. Digital Video Disc");
					System.out.println("4. Back");
					System.out.print("Enter your choice: ");
					choice = Integer.parseInt(keyboard.nextLine());
					switch(choice) {
					case 1:				// Book
						System.out.print("Enter the title: ");
						title = keyboard.nextLine();
						System.out.print("Enter the category: ");
						category = keyboard.nextLine();
						System.out.print("Enter the cost: ");
						cost = Float.parseFloat(keyboard.nextLine());
						//System.out.println("test: " + title + " - "  + category + " - " + cost);		//test
						System.out.print("Enter the number of authors: ");		// Enter authors
						numberAuthors = Integer.parseInt(keyboard.nextLine());
						for (int i = 0; i < numberAuthors; i++) {
							System.out.print("Enter the author: ");
							author = keyboard.nextLine();
							authors.add(author); 
						}
						Book book = new Book(title, category, cost, authors);
						testOrder.addMedia(book);			// add book to the order list
						break;
					case 2:				// Compact Disc

						System.out.print("Enter the title: ");
						title = keyboard.nextLine();
						System.out.print("Enter the category: ");
						category = keyboard.nextLine(); 
						System.out.print("Enter the cost: ");
						cost = Float.parseFloat(keyboard.nextLine());
						System.out.print("Enter the director: ");
						director = keyboard.nextLine();
						//System.out.print("Enter the artist: ");
						//artist = keyboard.nextLine();  
						CompactDisc compactDisc = new CompactDisc(title, category, cost, director);	// Make new Compact Disc
						System.out.print("Enter the number of tracks: ");			// Enter tracks
						numberTracks = Integer.parseInt(keyboard.nextLine());
						for (int i = 0; i < numberTracks; i++) {
							System.out.print("Enter the title of " + (i + 1) + " track: ");
							title = keyboard.nextLine();
							System.out.print("Enter the length of " + (i + 1) + " track: ");
							length = Integer.parseInt(keyboard.nextLine());
							Track track = new Track(title, length);
							compactDisc.addTrack(track);		// add track to the Compact Disc
							
						}
						testOrder.addMedia(compactDisc);		// add compactDisc to the testOrder
						collection.add(compactDisc);			// add compactDisc to the collection
						
						System.out.print("Do you want to play it? (1.Yes --- Other. No): ");
						choice = Integer.parseInt(keyboard.nextLine());
						if(choice == 1) {
							compactDisc.play();
						}
						
						break;
					case 3:				// Digital Video Disc
						System.out.print("Enter the title: ");
						title = keyboard.nextLine();
						System.out.print("Enter the category: ");
						category = keyboard.nextLine(); 
						System.out.print("Enter the cost: ");
						cost = Float.parseFloat(keyboard.nextLine());
						System.out.print("Enter the director: ");
						director = keyboard.nextLine();
						System.out.print("Enter the length: ");
						length = Integer.parseInt(keyboard.nextLine());
						DigitalVideoDisc digitalVideoDisc = new DigitalVideoDisc(title, category, cost, length, director);	// Make new Digital Video Disc
						testOrder.addMedia(digitalVideoDisc);
						System.out.print("Do you want to play it? (1.Yes --- Other. No): ");
						choice = Integer.parseInt(keyboard.nextLine());
						if(choice == 1) {
							digitalVideoDisc.play();
						}
						break;
					case 4:				// Back
						// print the sorted by number of tracks order list 
						Collections.sort((List)collection);
						
						System.out.println("Print the sorted by number of tracks order list: ");
						for(CompactDisc i: collection) {
							System.out.println(i.getTitle() + " - " + i.getTracks().size());
						}


						break;
					default:
						System.out.println("Fail! Please try again!");
					}						
				} while (choice != 4);
				break;
			case 3:		// delete item by title
				System.out.println("Enter the title of item you want to delete: ");
				title = keyboard.nextLine();
				testOrder.removeMedia(title);
				break;
			case 4: 	// Display the items list of order
				testOrder.printList();		// Display the list
				testOrder.printTotalCost(); // Display the total Cost
				//System.out.println(testOrder.getItemsOrdered().get(0).getArtist());
				break;
			case 0: 	//Exit
				break;
			default: 
				System.out.println("Fail! Please try again!");
			}
		} while (choice != 0);
		
		
		
		keyboard.close();
		
		
		
		
		
		
		/*
		Order anOrder = new Order();
		//create a new DVD object and set the fields
		DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King", "Animation", 19.95f, 87, "Roger Allers");
		//add the dvd to the order
		
		DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars", "Science Fiction", 24.95f, 124, "George Lucas");
		//add the dvd to the order
		
		DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdin", "Animation", 18.99f, 90, "John Musker");
		//add the dvd to the order
		
		Collection collection = new ArrayList();
		collection.add(dvd1);
		collection.add(dvd2);
		collection.add(dvd3);
		
		// Interator through the ArrayList and output their titles
		// (unsorted order)
		Iterator iterator = collection.iterator();
		
		System.out.println("------------------------------------");
		System.out.println("The DVDs currently in the order are: ");
		
		// print the order list (haven't sorted yet)
		while(iterator.hasNext()) {
			DigitalVideoDisc tmp = (DigitalVideoDisc)iterator.next();
			System.out.println(tmp.getTitle() + " - " + tmp.getCost());
			//System.out.println(((DigitalVideoDisc)iterator.next()).getTitle() + " - " + ((DigitalVideoDisc)iterator.);
		}
		
		// Sort the collection of DVDs - based on the compareTo() method
		Collections.sort((List)collection);
		
		// Iterate through the ArrayList and output their titles - in sorted order
		iterator = collection.iterator();
		System.out.println("------------------------------------");
		System.out.println("The DVDs in sorted order are: ");
	
		// print the sorted order list
		while(iterator.hasNext()) {
			DigitalVideoDisc tmp = (DigitalVideoDisc)iterator.next();
			System.out.println(tmp.getTitle() + " - " + tmp.getCost());
		}
		System.out.println("------------------------------------");
		
		*/
	}		
	
	
	public static void showMenu() {
		System.out.println("Order Management Application: ");
		System.out.println("--------------------------------");
		System.out.println("1. Create new order");
		System.out.println("2. Add item to the order");
		System.out.println("3. Delete item by title");
		System.out.println("4. Display the items list of order");
		System.out.println("0. Exit");
		System.out.println("--------------------------------");
		System.out.print("Please choose a number: 0-1-2-3-4:  ");
	}
}	
