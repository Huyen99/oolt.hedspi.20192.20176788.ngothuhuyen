package media;

public class Track implements Playable, Comparable<Track> {

	private String title;
	private int length;
	
	public void play() {
		System.out.println("Playing DVD: " + this.getTitle());
		System.out.println("DVD length: " + this.getLength());
	}

	//constructor
	public Track(String title, int length) {
		this.setTitle(title);
		this.setLength(length);
	}

	@Override
	public int compareTo(Track o) {
		// sort by title
		return title.compareTo(o.title);
	}

	
	// getter and setter
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

}
