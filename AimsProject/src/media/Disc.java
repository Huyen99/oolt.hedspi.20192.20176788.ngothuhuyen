package media;

public class Disc extends Media {

	public int length;
	public String director;
	
	// Constructor
	public Disc(String title, String category, float cost, int length, String director) {
		super(title, category, cost);
		this.setLength(length);
		this.setDirector(director);
	}
	
	// Constructor for Compact Disc
	public Disc(String title, String category, float cost, String director) {
		super(title, category, cost);
		this.setDirector(director);
	}
	
	
	public Disc() {
		
	}


	// getter and setter
	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}
	
	

}
