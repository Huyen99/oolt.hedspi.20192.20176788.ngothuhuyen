package media;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Book extends Media implements Comparable<Book>{
	private List<String> authors = new ArrayList<String>();
	private String content;
	List<String> contentTokens = new ArrayList<String>();
	HashMap<String, Integer> wordFrequency= new HashMap<String, Integer>();


	//constructor
	public Book(String title) {
		super(title);
	}
	
	public Book(String title, String category) {
		super(title, category);
	}
	
	public Book(String title, String category, float cost, List<String> authors) {
		super(title, category, cost);
		this.authors = authors;
	}
	
	public Book(String title, String category, float cost, List<String> authors, String content) {
		super(title, category, cost);
		this.authors = authors;
		this.content = content;
	}
	
	public Book(String title, String category, float cost) {
		super(title, category, cost);
	}
		
	public Book() {
		// TODO Auto-generated constructor stub
	}
	
	

	public void addAuthor(String authorName) {
		for(String i: authors) {
			if(i.equals(authorName) == true) {
				System.out.println("This author is existing in list!");
				return;
			} 
		}
		authors.add(authorName);
	}
	
	public void removeAuthor(String authorName) {
		for(String i: authors) {
			if(i.equals(authorName) == true) {
				System.out.println("This author was deleted!");
				authors.remove(i);
				return;
			} 
		}
		System.out.println("This author doesn't exist in the list!");
	}

	public void processContent() {
		String tmpContentToken = "";
		int i = 0;
		int j = 0;
		int key = 0;
		List<Character> punctuations = new ArrayList<Character>();
		punctuations.add(' ');
		punctuations.add('\n');
		punctuations.add(';');
		punctuations.add(':');
		punctuations.add('\'');
		punctuations.add('\\');
		punctuations.add(',');
		punctuations.add('.');
		punctuations.add('?');
		punctuations.add('!');
		punctuations.add('(');
		punctuations.add(')');
		punctuations.add('-');
		punctuations.add('\"');
		punctuations.add('/');
		punctuations.add('[');
		punctuations.add(']');
		punctuations.add('{');
		punctuations.add('}');
		//punctuations.add('\EOF');

		
		// split and add contentToken in contentTokens list and HashMap
		for(j = i; j < content.length(); j++) {
			if(punctuations.contains(content.charAt(j)) || j == content.length() - 1) {		//check if content is punctuation or not
				if (key == 1) {									// check if it has content (not only ' ' and '\n')
					key = 0;
					if (j == content.length() - 1) {			// check if is this the end of content
						tmpContentToken = content.substring(i, j + 1);
					} else 	tmpContentToken = content.substring(i, j);
					i = j + 1;
					//j++;
					if (contentTokens.contains(tmpContentToken) == false) {
						contentTokens.add(tmpContentToken);		// add contentToken to List<String> contentTokens
						wordFrequency.put(tmpContentToken, 1);	// add contentToken to wordFrequency HashMap
					} else {									// count the number of token
						int number = wordFrequency.get(tmpContentToken) + 1;
						wordFrequency.replace(tmpContentToken, number);
					}
				} else i++;
			} else key = 1; 			// key = 1 if token isn't punctuation
		}
	}
	
	
	
	
	
	@Override
	public int compareTo(Book o) {
		// TODO Auto-generated method stub
		
		return super.getTitle().compareTo(o.getTitle());
	}
	

	@Override
	public String toString() {
		System.out.println("Title: " + super.getTitle());
		System.out.println("Category: " + super.getCategory());
		System.out.println("Cost: " + super.getCost());
		System.out.print("List of authors: ");
		for (int i = 0; i < authors.size(); i++) {				// print the list of authors
			if (i == authors.size() - 1) {
				System.out.print(authors.get(i) + "\n");
			} else System.out.print(authors.get(i) + " - ");
		}
		System.out.println("The content of the book: " + getContent());
		System.out.println("The contentTokens of book: ");		// print the list of tokens
		for(int i = 0; i < contentTokens.size(); i++) {
			System.out.println("\t" + contentTokens.get(i) + " - " + wordFrequency.get(contentTokens.get(i)));
		}
		
		
		return "a";
	}
	

	// getter and setter
	
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public List<String> getAuthors() {
		return authors;
	}

	public void setAuthors(List<String> authors) {
		this.authors = authors;
	}


}
