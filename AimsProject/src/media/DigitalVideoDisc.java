package media;

public class DigitalVideoDisc extends Disc implements Playable, Comparable<DigitalVideoDisc> {
	private int length;
	private String director;
	
	//constructor

	
	public DigitalVideoDisc(String title, String category, float cost, int length, String director) {
		super(title, category, cost, length, director);
	}

	
	public void play() {
		System.out.println("Playing DVD: " + this.getTitle());
		System.out.println("DVD length: " + this.getLength());
	}


	public boolean search (String title) {
		String[] tempToken = new String[10];
		int i_tk = 0;
		int k1 = 0;
		int k2 = 0;
		for(int i = 0; i < title.length(); i++) {
			if(title.charAt(i) == ' ') {
				k2 = i;
				tempToken[i_tk] = new String(title.substring(k1, k2));
				k1 = i + 1;
				i_tk++;
				System.out.println(i_tk + "   " + tempToken[i_tk - 1]);
			} else if(i == title.length() - 1) {
				k2 = i;
				tempToken[i_tk] = new String(title.substring(k1, k2 + 1));
				System.out.println(i_tk + 1 + "   " + tempToken[i_tk]);
			}
		}
		
		for(int i = 0; i < i_tk; i++) {
			if(title.contains(tempToken[i]) == true) {
				return true;
			}
		}
		return false;
	}


	@Override
	public int compareTo(DigitalVideoDisc o) {
		// sort by title
		//return super.getTitle().compareTo(o.getTitle());
		// sort by cost
		return Double.compare(super.getCost(), o.getCost());
	}
	
	
	// getter and setter
	/*
	public String getDirector() {
		return director;
	}
	public void setDirector(String director) {
		this.director = director;
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	
	*/
}
